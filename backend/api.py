from flask import Flask, jsonify, abort, make_response, request, url_for


app = Flask(__name__)


weine = [
    {"id": 1, "name": "Huxelrebe", "rating": "1"},
    {"id": 2, "name": "Morio Muskat", "rating": "2"},
    {"id": 3, "name": "Chardonney", "rating": "5"},
    {"id": 4, "name": "Sauvignon Blanc", "rating": "4"},
    {"id": 5, "name": "Grauburgunder", "rating": "3"},
    {"id": 6, "name": "Muskateller", "rating": "3"},
    {"id": 7, "name": "Muskateller", "rating": "3"},
    {"id": 8, "name": "Muskateller", "rating": "3"},
    {"id": 9, "name": "Muskateller", "rating": "3"},
    {"id": 10, "name": "Muskateller", "rating": "3"},
    {"id": 11, "name": "Muskateller", "rating": "3"},
    {"id": 12, "name": "Muskateller", "rating": "3"},
    {"id": 13, "name": "Muskateller", "rating": "3"},
    {"id": 14, "name": "Muskateller", "rating": "3"},
    {"id": 15, "name": "Muskateller", "rating": "3"},
    {"id": 16, "name": "Muskateller", "rating": "3"},
]


@app.errorhandler(404)
def not_found(error):
    return make_response(jsonify({"error": "Not found"}), 404)


@app.route("/weincheckerapp/api/v1.0/weine", methods=["GET"])
def get_weine():
    return jsonify([make_public_wein(wein) for wein in weine]), 200


@app.route("/weincheckerapp/api/v1.0/weine/<int:wein_id>", methods=["GET"])
def get_wein(wein_id):
    wein = [wein for wein in weine if wein["id"] == wein_id]
    if len(wein) == 0:
        abort(404)
    return jsonify({"wein": make_public_wein(wein[0])})


@app.route("/weincheckerapp/api/v1.0/weine", methods=["POST"])
def create_wein():
    if not request.json or not "name" or not "rating" in request.json:
        abort(400)
    wein = {
        "id": weine[-1]["id"] + 1,
        "name": request.json["name"],
        "rating": request.json["rating"],
    }
    weine.append(wein)
    return jsonify({"wein": wein}), 201


@app.route("/weincheckerapp/api/v1.0/weine/<int:wein_id>", methods=["PUT"])
def update_wein(wein_id):
    wein = [wein for wein in weine if wein["id"] == wein_id]
    if len(wein) == 0:
        abort(404)
    if not request.json:
        abort(400)
    if "name" in request.json and type(request.json["title"]) != unicode:
        abort(400)
    if "rating" in request.json and type(request.json["rating"]) is not int:
        abort(400)
    wein[0]["name"] = request.json.get("name", wein[0]["name"])
    wein[0]["rating"] = request.json.get("rating", wein[0]["rating"])
    return jsonify({"wein": wein[0]})


@app.route("/weincheckerapp/api/v1.0/weine/<int:wein_id>", methods=["DELETE"])
def delete_wein(wein_id):
    wein = [wein for wein in weine if wein["id"] == wein_id]
    if len(wein) == 0:
        abort(404)
    weine.remove(wein[0])
    return jsonify({"result": True})


def make_public_wein(wein):
    new_wein = {}
    for field in wein:
        if field == "id":
            new_wein["uri"] = url_for("get_wein", wein_id=wein["id"], _external=True)
        else:
            new_wein[field] = wein[field]
    return new_wein


if __name__ == "__main__":
    app.run(debug=True)
