# Funktionen

## (1) Barcode scannen

### Use case

- Nutzer möchte Informationen über einen Wein erhalten
- Nutzer möchte wissen ob ein Wein schon in der Datenbank ist

### Ablauf

1. App öffnen
2. Auf Screen tippen
3. Barcode scannen
4. Ergebnis  
    1. App zeigt Infoblatt
    2. App zeigt Popup mit Frage, ob Wein hinzufügen

## (2) Infos/Bewertung zu Wein lesen

### Use case

- Nutzer möchte Eigenschaften eines Weines erfahren
- Nutzer möchte die Bewertung der Community zu einem Wein erfahren

### Ablauf

1. Nutzer gelangt durch (1) auf Infoblatt eines Weines
2. Nutzer sieht Eigenschaften und durchschnittliche Bewertung
3. Nutzer öffnet detaillierte Bewertungen
4. Nutzer liest detaillierte Bewertungen

## (3) Wein hinzufügen

### Use case

- Nutzer möchte einen Wein hinzufügen weil er geschmeckt hat, oder er die Community berreichern oder warnen möchte

### Ablauf

1. Nutzer führt (1) aus
2. Wein ist nicht in Datenbank
3. Nutzer bekommt Popup Frage, um Wein hinzuzufügen
4. Nutzer bestätigt
5. Nutzer gelangt auf Formularseite
6. Nutzer füllt Formular aus
7. Nutzer bestätigt Formular

## (4) Wein bewerten

### Use case

### Ablauf
