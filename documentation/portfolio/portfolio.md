# Implementierung einer mobilen Appllikation

## 1. Idee

Wir möchten eine Plattform für Studis bieten verschiedene Weine, vorerst im Raum Mannheim, zu bewerten.

## 2. Anforderungen

- Wein finden
- Wein bewerten

## 3. Funktionen

1. Barcode scannen
2. Wein suchen
3. Weinliste filtern
4. Wein bewerten
5. Wein hinzufügen

## 4. Architektur

### 4.1 Context View

![Context](images/Views-Context.png)

### 4.2 Functional View

![Functional](images/Views-Functional.png)

### 4.3 Hardware View

![Hardware](images/Views-Hardware.png)

## 5. Flutter

### 5.1 Was ist Flutter?

### 5.2 Warum Flutter?

### 5.3 Ein App mit Flutter programmieren

## 6. Flask REST API

### 6.1 Was ist Flask?

### 6.2 Einen REST API programmieren

## 7. Postgresql

## 8. Docker
