# Architectural Drivers

## Business goals

- build platform to share experience with different wines
- No active selling
- establish community to share experience with different wines
  - User interaction search/add/rate (context view)
- provide all information needed to choose a wine
  - User > add
  - supplier interaction > provide price/attributes (context view)

## Functional requirements

- get information of wine
  - search by name/barcode (functional view)
- share experience
  - User > rate/comment wine (context view)

## Quality requirements

- indepenency
  - platform from scratch (hardware view)
- secure from manipulation 
- no advertising
  - supplier can't add wine only add attributes after wine was added (context view)

## Constraints

- performance restricted by internet connection
  - connection to remote server (api/db) needed (hardware view)