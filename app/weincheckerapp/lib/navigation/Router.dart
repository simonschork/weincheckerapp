import 'package:flutter/material.dart';
import 'package:WeinCheckerApp/navigation/Constants.dart';
import 'package:WeinCheckerApp/screens/FavScreen.dart';
import 'package:WeinCheckerApp/screens/HomeScreen.dart';
import 'package:WeinCheckerApp/screens/SearchScreen.dart';

class Router {
  static Route<dynamic> createRoute(RouteSettings settings) {
    switch (settings.name) {
      case homeRoute:
        return MaterialPageRoute(builder: (_) => HomeScreen());
      case searchRoute:
        return MaterialPageRoute(builder: (_) => SearchScreen());
      case favRoute:
        return MaterialPageRoute(builder: (_) => FavScreen());
      default:
        return MaterialPageRoute(
          builder: (_) => Scaffold(
            body: Center(
              child: Text(
                "No route defined for ${settings.name}",
              ),
            ),
          ),
        );
    }
  }
}
