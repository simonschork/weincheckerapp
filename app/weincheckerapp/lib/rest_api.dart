import 'dart:convert';
import 'package:WeinCheckerApp/model/wein_model.dart';
import 'package:http/http.dart' as http;

class URLS {
  static const String BASE_URL =
      "http://localhost:5000/weincheckerapp/api/v1.0";
}

class ApiService {
  static Future<List<Wein>> getWeine() async {
    final response = await http.get("${URLS.BASE_URL}/weine");

    if (response.statusCode == 200) {
      List jsonResponse = json.decode(response.body);
      return jsonResponse.map((wein) => Wein.fromJson(wein)).toList();
    } else {
      throw Exception("Failed to loading API");
    }
  }
}
