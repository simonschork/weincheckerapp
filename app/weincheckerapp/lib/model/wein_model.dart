// class WeinListe {
//   final List<Wein> weine;

//   WeinListe({this.weine});

//   factory WeinListe.fromJson(List<dynamic> parsedJson) {
//     List<Wein> weine = List<Wein>();
//     weine = parsedJson.map((wein) => Wein.fromJson(wein)).toList();

//     return WeinListe(
//       weine: weine,
//     );
//   }
// }

class Wein {
  final String name;
  final String rating;
  final String uri;

  Wein({this.name, this.rating, this.uri});

  factory Wein.fromJson(Map<String, dynamic> parsedjson) {
    return Wein(
      name: parsedjson["name"],
      rating: parsedjson["rating"].toString(),
      uri: parsedjson["uri"],
    );
  }
}
