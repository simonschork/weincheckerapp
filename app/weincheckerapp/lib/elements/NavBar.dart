import 'package:barcode_scan/barcode_scan.dart';
import 'package:flutter/material.dart';
import 'package:WeinCheckerApp/navigation/Constants.dart';
import 'package:WeinCheckerApp/utils/SizeConfig.dart';
import 'package:flutter/services.dart';

class HomeNavBar extends StatefulWidget {
  @override
  _HomeNavBarState createState() => _HomeNavBarState();
}

class _HomeNavBarState extends State<HomeNavBar> {
  String barcode = "";

  Future scan() async {
    try {
      String barcode = await BarcodeScanner.scan();
      setState(() {
        this.barcode = barcode;
      });
    } on PlatformException catch (e) {
      if (e.code == BarcodeScanner.CameraAccessDenied) {
        setState(() {
          this.barcode = "Camera permission not granted";
        });
      } else {
        setState(() {
          this.barcode = "Unkown error: $e";
        });
      }
    } on FormatException {
      setState(() {
        this.barcode = "User abort scan";
      });
    } catch (e) {
      setState(() {
        this.barcode = "Unknown error: $e";
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: Padding(
        padding: EdgeInsets.only(bottom: SizeConfig.safeBlockHorizontal * 5),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.end,
          children: <Widget>[
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                Expanded(
                  child: IconButton(
                    icon: Icon(Icons.search),
                    color: Colors.white,
                    onPressed: () =>
                        Navigator.pushReplacementNamed(context, searchRoute),
                    iconSize: SizeConfig.safeBlockVertical * 5,
                  ),
                ),
                Ink(
                  decoration: ShapeDecoration(
                      shape: CircleBorder(), color: Colors.white),
                  child: IconButton(
                    icon: Icon(Icons.camera_alt),
                    color: Colors.grey[800],
                    onPressed: () => scan(),
                    iconSize: SizeConfig.safeBlockVertical * 6,
                  ),
                ),
                Expanded(
                  child: IconButton(
                    icon: Icon(Icons.star),
                    color: Colors.white,
                    onPressed: () =>
                        Navigator.pushReplacementNamed(context, favRoute),
                    iconSize: SizeConfig.safeBlockVertical * 5,
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}

class SearchNavBar extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: Padding(
        padding: EdgeInsets.only(bottom: SizeConfig.safeBlockHorizontal * 5),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.end,
          children: <Widget>[
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                Expanded(
                  child: Ink(
                    decoration: ShapeDecoration(
                      shape: CircleBorder(),
                      color: Colors.white,
                    ),
                    child: IconButton(
                      icon: Icon(Icons.search),
                      color: Colors.grey[800],
                      onPressed: () => print("SearchButtonPressed"),
                      iconSize: SizeConfig.safeBlockVertical * 6,
                    ),
                  ),
                ),
                IconButton(
                  icon: Icon(Icons.home),
                  color: Colors.white,
                  onPressed: () =>
                      Navigator.pushReplacementNamed(context, homeRoute),
                  iconSize: SizeConfig.safeBlockVertical * 5,
                ),
                Expanded(
                  child: IconButton(
                    icon: Icon(Icons.star),
                    color: Colors.white,
                    onPressed: () =>
                        Navigator.pushReplacementNamed(context, favRoute),
                    iconSize: SizeConfig.safeBlockVertical * 5,
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}

class FavNavBar extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: Padding(
        padding: EdgeInsets.only(bottom: SizeConfig.safeBlockHorizontal * 5),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.end,
          children: <Widget>[
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                Expanded(
                  child: IconButton(
                    icon: Icon(Icons.search),
                    color: Colors.white,
                    onPressed: () =>
                        Navigator.pushReplacementNamed(context, searchRoute),
                    iconSize: SizeConfig.safeBlockVertical * 5,
                  ),
                ),
                IconButton(
                  icon: Icon(Icons.home),
                  color: Colors.white,
                  onPressed: () =>
                      Navigator.pushReplacementNamed(context, homeRoute),
                  iconSize: SizeConfig.safeBlockVertical * 5,
                ),
                Expanded(
                  child: Ink(
                    decoration: ShapeDecoration(
                      shape: CircleBorder(),
                      color: Colors.white,
                    ),
                    child: IconButton(
                      icon: Icon(Icons.star),
                      color: Colors.grey[800],
                      onPressed: () => print("FavButtonPressed"),
                      iconSize: SizeConfig.safeBlockVertical * 6,
                    ),
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
