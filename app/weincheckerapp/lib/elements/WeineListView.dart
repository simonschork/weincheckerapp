import 'package:WeinCheckerApp/model/wein_model.dart';
import 'package:WeinCheckerApp/rest_api.dart';
import 'package:WeinCheckerApp/utils/SizeConfig.dart';
import 'package:flutter/material.dart';

class WeineListView extends StatefulWidget {
  @override
  _WeineListViewState createState() => _WeineListViewState();
}

class _WeineListViewState extends State<WeineListView> {
  Widget build(BuildContext context) {
    return FutureBuilder<List<Wein>>(
      future: ApiService.getWeine(),
      builder: (context, snapshot) {
        if (snapshot.hasData) {
          List<Wein> weine = snapshot.data;
          return _weineListView(weine);
        } else if (snapshot.hasError) {
          return Text("${snapshot.error}");
        }
        return CircularProgressIndicator();
      },
    );
  }

  ListView _weineListView(data) {
    return ListView.builder(
        itemCount: data.length,
        itemBuilder: (context, index) {
          return _tile(data[index].name, data[index].rating);
        });
  }

  ListTile _tile(String name, String rating) => ListTile(
        title: Text(
          name,
          style: TextStyle(
            color: Colors.white,
            fontSize: SizeConfig.safeBlockHorizontal * 6,
          ),
        ),
        subtitle: Text(
          "Rating: $rating",
          style: TextStyle(
            color: Colors.white,
            fontSize: SizeConfig.blockSizeHorizontal * 5,
          ),
        ),
        leading: Padding(
          padding: EdgeInsets.only(
            top: SizeConfig.blockSizeHorizontal,
            bottom: SizeConfig.blockSizeHorizontal,
          ),
          child: Icon(
            Icons.local_bar,
            color: Colors.white,
            size: SizeConfig.blockSizeHorizontal * 10,
          ),
        ),
      );
}
