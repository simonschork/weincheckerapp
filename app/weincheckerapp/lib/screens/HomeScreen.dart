import 'package:WeinCheckerApp/elements/NavBar.dart';
import 'package:flutter/material.dart';
import 'package:WeinCheckerApp/utils/SizeConfig.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Scaffold(
      backgroundColor: Colors.grey[800],
      body: SafeArea(
        minimum: EdgeInsets.only(
          left: SizeConfig.safeBlockVertical,
          right: SizeConfig.safeBlockVertical,
        ),
        child: Column(
          children: <Widget>[
            Padding(
              padding:
                  EdgeInsets.only(top: SizeConfig.safeBlockHorizontal * 25),
              child: Column(
                children: <Widget>[
                  Center(
                    child: Text(
                      "Willkommen bei der\n WeinCheckerApp!",
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: SizeConfig.safeBlockHorizontal * 7,
                      ),
                      textAlign: TextAlign.center,
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.only(
                        top: SizeConfig.safeBlockHorizontal * 10),
                    child: Center(
                      child: Text(
                        "Möchtest du schnell Infos über einen Wein erhalten, oder einen Wein hinzufügen drücke den Scanbutton.\n\nMöchtest du einen Wein\n per Namen suchen,\n oder einfach die Liste durchscrollen drücke den Suchenbutton.\n\nMöchtest du deine Bewertungen durchschauen und Weine finden,\ndie dir gefallen haben,\n drücke den Favoritenbutton.",
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: SizeConfig.safeBlockHorizontal * 5,
                        ),
                        textAlign: TextAlign.center,
                      ),
                    ),
                  ),
                ],
              ),
            ),
            HomeNavBar(),
          ],
        ),
      ),
    );
  }
}
