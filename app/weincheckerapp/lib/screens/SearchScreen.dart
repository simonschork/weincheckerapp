import 'package:WeinCheckerApp/elements/NavBar.dart';
import 'package:WeinCheckerApp/elements/WeineListView.dart';
import 'package:flutter/material.dart';
import 'package:WeinCheckerApp/utils/SizeConfig.dart';

class SearchScreen extends StatefulWidget {
  @override
  _SearchScreenState createState() => _SearchScreenState();
}

class _SearchScreenState extends State<SearchScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey[800],
      body: SafeArea(
        minimum: EdgeInsets.only(
          left: SizeConfig.safeBlockVertical,
          right: SizeConfig.safeBlockVertical,
        ),
        child: Column(
          children: <Widget>[
            Padding(
              padding:
                  EdgeInsets.only(top: SizeConfig.blockSizeHorizontal * 30),
              child: Container(
                height: SizeConfig.blockSizeHorizontal * 140,
                child: WeineListView(),
              ),
            ),
            SearchNavBar(),
          ],
        ),
      ),
    );
  }
}
