import 'package:WeinCheckerApp/elements/NavBar.dart';
import 'package:flutter/material.dart';
import 'package:WeinCheckerApp/utils/SizeConfig.dart';

class FavScreen extends StatefulWidget {
  @override
  _FavScreenState createState() => _FavScreenState();
}

class _FavScreenState extends State<FavScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey[800],
      body: SafeArea(
        minimum: EdgeInsets.only(
          left: SizeConfig.safeBlockVertical,
          right: SizeConfig.safeBlockVertical,
        ),
        child: FavNavBar(),
      ),
    );
  }
}
