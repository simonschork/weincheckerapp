import 'package:flutter/material.dart';
import 'package:WeinCheckerApp/navigation/Constants.dart';
import 'package:WeinCheckerApp/navigation/Router.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'WeinCheckerApp',
      onGenerateRoute: Router.createRoute,
      initialRoute: homeRoute,
    );
  }
}
