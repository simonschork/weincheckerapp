# WeinCheckerApp
## Repository Structure
.  
|- app // Frontend implementation folder  
 |- - weincheckerapp // Flutter project folder  
|- backend // Backend with Python-Flask-REST-API  
|- design // Mockups and design drawings  
|- documentation // Contains all documentation  
